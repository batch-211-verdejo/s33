// What is an API?
/* 
    The part of a server responsible for receiving requests and sending responses
        - Just as a bank teller stands between a customer and the bank's vault, an API acts as the middleman between the front and back ends of an application

*/
// What is it for?
/* 
    Hides the server beneath an interface layer
        - Hidden complexity makes apps easier to use
        - Sets the rules of interaction between front and back ends of an application, improving security

*/
// What is REST
/* 
    REpresentational State Transfer
        - An architectural style for providing standards for communication between computer systems

*/
// How does it sove the problem?
/* 
    Statelessness
        - Server does not need to know client state and vice versa
        - Every client request has all the information it needs to be processed by the API
        - Made possible via resources and HTTP methods

    Standardized Communication
        - Enables a decoupled server to understand, process, and respond to client requests WITHOUT knowing client state
        - Implemented via resources represented as Uniform Resource Identifier (URI) endpoints and HTTP methods
            - Resources are plural by convention
                ex.
                    - /api/photos(NEVER/api/photo)
                    - /api/statuses

*/
// Anatomy of a Client Request
/* 
    - Operation to be performed dictated by HTTP methods
        - GET - receive information about an API resource (Retrieve/Find in MongoDB)
        - POST - Create an API resource (Create/insert in MongoDB)
        - PUT - Update an API resource (Update in MongoDB)
        - DELETE - Deletes a resource
    
    - A path to the resource to be operated on
            - URI endpoint
                ex http://localhost:4000/api/tasks/sad6f1sa6df1as6d5f16s1df6

    - A header containing additional information
    
    - A request body containing data (optional)

*/